import React from "react";
import ReactDOM from "react-dom";
import Main from "./app/components/main/js/app.jsx";
import WebFont from 'webfontloader';

WebFont.load({
    google: {
        families: ['Open Sans:300,400,700', 'sans-serif']
    },
    custom: {
        families: ['My Font', 'My Other Font:n4,i4,n7'],
        urls: ['']
    }
});

ReactDOM.render(<Main /> , document.getElementById('app'));