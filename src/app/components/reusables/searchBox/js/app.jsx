import React from 'react';
import TextBox from "../../textBox/js/app.jsx";
import "../css/app.less";
export default class SearchBox extends React.Component {
    render () {
        return (
            <div className="search-box">
                <TextBox placeholder={this.props.placeholder}/>
            </div>
        )
    }
}