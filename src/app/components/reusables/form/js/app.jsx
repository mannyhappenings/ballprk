import React from "react";

export default class Form extends React.Component{
    render() {
        return (
            <div className={"form " + this.props.classes.join(" ")}>
                <form>
                    {this.props.children}
                </form>
            </div>
        );
    }
}