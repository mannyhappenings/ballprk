import React from "react";
import "../css/app.css";

export default class TextBox extends React.Component {
    render () {
        return <input className="textbox" type="text" placeholder={this.props.placeholder} />
    }
}