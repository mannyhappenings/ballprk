import React from "react";
import "../css/app.less";

export default class FormLabel extends React.Component {
    render () {
        return (
            <label className="form-label" htmlFor={this.props.for}><span className="label-text">{this.props.labelText}</span>{this.props.children}</label>
        );
    }
}