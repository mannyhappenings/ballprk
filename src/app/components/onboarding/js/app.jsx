import React from "react";
import Header from "../../header/js/app.jsx";
import FormLabel from "../../reusables/formLabel/js/app.jsx";
import TextBox from "../../reusables/textBox/js/app.jsx";
import Form from "../../reusables/form/js/app.jsx";
import "../css/app.less";

export default class Onboarding extends React.Component {
    render () {
        return (
            <div>
                <Header>
                    <div className="logout-button">
                        LOGOUT
                    </div>
                </Header>
                <div className="onboarding-container">
                    <div className="left-container">
                        <div className="onboarding-form-container">
                            <div className="message-1">Let's get started</div>
                            <div className="message-2">We saw across the room and we don't usually do this but...</div>
                            <div className="back-button"><i className="back-icon"></i><span className="prep-school">Prep School</span></div>
                            <Form classes={["onboarding-form"]}>
                                <FormLabel labelText="Sport" for="sport">
                                    <TextBox name="sport" placeholder="Baseball"/>
                                </FormLabel>
                                <FormLabel labelText="Position" for="position"><TextBox name="position" placeholder="Chose your position"/></FormLabel>
                                <FormLabel labelText="High School" for="high_school"><TextBox name="high_school"/></FormLabel>
                                <FormLabel labelText="Birth Name" for="birth_name"><TextBox name="birth_name"/></FormLabel>
                                <FormLabel labelText="Gender" for="gender"><TextBox name="gender"/></FormLabel>
                                <FormLabel labelText="Pronoun" for="pronoun"><TextBox name="pronoun" placeholder="What pronoun, do you prefer?"/></FormLabel>
                            </Form>
                        </div>
                    </div>
                    <div className="right-container">

                    </div>
                </div>
            </div>
        );
    }
}