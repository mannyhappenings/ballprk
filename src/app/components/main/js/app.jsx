import React from 'react';
import {Router, Route, Switch} from 'react-router-dom';
import Onboarding from "../../onboarding/js/app.jsx";
import Partners from "../../partners/js/app.jsx";
import createBrowserHistory from "history/createBrowserHistory";
import "../css/app.less";

export default () => (
    <Router history={createBrowserHistory()}>
        <Switch>
            <Route exact path="/onboarding" component={Onboarding}/>
            <Route path="/partners/:name" component={Partners}/>
        </Switch>
    </Router>
);