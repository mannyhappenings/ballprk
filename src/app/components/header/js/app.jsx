import React from "react";
import "../css/app.less"

export default class Header extends React.Component {
    render () {
        return (
            <header>
                <div className="logo">
                    <span>B A L L P R K</span>
                </div>
                <div className="header-children">
                    {this.props.children}
                </div>
            </header>
        );
    }
}