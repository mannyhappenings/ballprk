import React from 'react';
import "../css/app.less";

export default class Athlete extends React.Component {
    render() {
        
        return (
            <div className="athlete-detail-thumb">
                <div className="athlete-image">
                    <img className="cover-pic" src={this.props.data.profile.coverPic} alt={this.props.data.profile.name}/>
                </div>
                <div className="display-pic">
                    <div className="display-image" style={{backgroundImage: 'url("' + this.props.data.profile.displayPic + '")'}}></div>
                </div>
                <div className="athlete-details-short">
                    <div className="athlete-name">{this.props.data.profile.name}</div>
                    <div className="athlete-sport">{this.props.data.profile.sport} | {this.props.data.profile.position}</div>
                    <div className="athlete-team">{this.props.data.profile.team}</div>
                </div>
                <div className="social-buttons">
                    <button className="message-button">Message</button>
                    <button className="book-button">Book Session</button>
                </div>
            </div>
        )
    }
}