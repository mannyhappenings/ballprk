import React from 'react';
import SearchBox from "../../../../../reusables/searchBox/js/app.jsx";
import athleteService from "../../../../../../services/athlete.js";
import Athelete from "../athlete/js/app.jsx";
import "../css/app.less";

export default class Athlete extends React.Component {
    constructor () {
        super();

        this.state = {
            loading: true
        };
    }

    componentWillMount () {
        athleteService.getAthletes()
            .then(athletes => {
                this.setState({
                    athletes
                }, () => {
                    this.setState({loading: false});
                });
            })
    }

    getLoadingPage() {
        <div className="loading-page">
            Loading Atheletes...
        </div>
    }

    render () {
        return (
            <div className="athlete-page-container">
                <div className="search-box-container">
                    <SearchBox placeholder="&#xF002;  Search Athletes"/>
                </div>
                <div className="athlete-results-container">
                    {
                        this.state.loading?
                            this.getLoadingPage():
                            this.state.athletes.map((athlete, index) => <Athelete data={athlete} key={index}/>)
                    }
                </div>
            </div>
        );
    }
}