import React from 'react';
import { NavLink } from 'react-router-dom';
import "../css/app.less";

export default class PartnerNav extends React.Component {
    render () {
        return (
            <div className="partner-nav nav-container">
                <div className="nav-link">
                    <NavLink className="nav-item partner-nav-item" activeClassName="active" to={`${this.props.match.url}/posts`}>Posts</NavLink>
                </div>
                <div className="nav-link">
                    <NavLink className="nav-item partner-nav-item" activeClassName="active" to={`${this.props.match.url}/athlete`}>Our Athletes</NavLink>
                </div>
                <div className="nav-link">
                    <NavLink className="nav-item partner-nav-item" activeClassName="active" to={`${this.props.match.url}/photos`}>Photos</NavLink>
                </div>
                <div className="nav-link">
                    <NavLink className="nav-item partner-nav-item" activeClassName="active" to={`${this.props.match.url}/videos`}>Videos</NavLink>
                </div>
                <div className="nav-link">
                    <NavLink className="nav-item partner-nav-item" activeClassName="active" to={`${this.props.match.url}/events`}>Events</NavLink>
                </div>
            </div>
        );
    }
}