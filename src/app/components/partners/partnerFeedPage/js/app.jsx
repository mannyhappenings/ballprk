import React from 'react';
import PartnerNav from '../partnerNav/js/app.jsx';
import {Router, Route, IndexRoute, Switch} from 'react-router-dom';
import { createBrowserHistory as createHistory } from "history";
import Posts from "../partnerNav/postsPage/js/app.jsx";
import Athlete from "../partnerNav/athletePage/js/app.jsx";
import Photos from "../partnerNav/photosPage/js/app.jsx";
import Videos from "../partnerNav/videosPage/js/app.jsx";
import Events from "../partnerNav/eventsPage/js/app.jsx";
import "../css/app.less";

export default class PartnerFeedPage extends React.Component {
    render () {
        return (
            <div className="partner-feed-page">
                <div className="partner-feed-container">
                    <PartnerNav
                        match={this.props.match} />
                    <div className="partner-subpage-container">
                        <Router history={createHistory()}>
                            <Switch>
                                <Route path={`${this.props.match.path}/posts`} component={Posts} />
                                <Route path={`${this.props.match.path}/athlete`} component={Athlete} />
                                <Route path={`${this.props.match.path}/photos`} component={Photos} />
                                <Route path={`${this.props.match.path}/videos`} component={Videos} />
                                <Route path={`${this.props.match.path}/events`} component={Events} />
                                <Route path={`${this.props.match.path}/`} component={Posts} />
                            </Switch>
                        </Router>
                    </div>
                </div>
            </div>
        );
    }
}