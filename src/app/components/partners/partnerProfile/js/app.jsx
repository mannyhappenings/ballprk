import React from 'react';
import StatBox from "../../../statBox/js/app.jsx";
import { Link } from 'react-router-dom'
import "../css/app.less";

export default class PartnerProfile extends React.Component {
    render() {
        return (
            <div className="partner-profile">
                <div className="display-details">
                    <div className="cover-photo">
                        <div style={{backgroundImage: 'url("' + this.props.profileDetails.coverPhoto + '")'}} alt={this.props.profileDetails.name} className="cover-photo"/>
                    </div>
                </div>
                <div className="partner-details">
                    <div className="partner-stats">
                        <p className="name">{this.props.profileDetails.name}</p>
                        <p className="location">{this.props.profileDetails.location}</p>
                        <StatBox stats={this.props.stats}/>
                    </div>
                    <Link className="website-link" to={this.props.profileDetails.website}>{this.props.profileDetails.website}</Link>
                    <div className="description">{this.props.profileDetails.description}</div>
                </div>
                <div className="partners">
                    <h3>Our Partners</h3>
                    <div className="partner-list">
                        {this.props.partners.map((partner, index) => (
                            <span className="partner-logo" key={index}>
                                <Link to={partner.link}><img src={partner.logo} alt={partner.name}/></Link>
                            </span>
                        ))}
                    </div>
                </div>
            </div>
        );
    }
}