import React from "react";
import { Link } from 'react-router-dom';
import Header from "../../header/js/app.jsx";
import FormLabel from "../../reusables/formLabel/js/app.jsx";
import TextBox from "../../reusables/textBox/js/app.jsx";
import Form from "../../reusables/form/js/app.jsx";
import SearchButton from "../../reusables/searchButton/js/app.jsx";
import ProfileDetails from "../../profileDetails/js/app.jsx";
import PartnerProfile from '../partnerProfile/js/app.jsx';
import PartnerFeedPage from "../partnerFeedPage/js/app.jsx";
import "../css/app.less";

class Partners extends React.Component {
    constructor () {
        super();

        this.clubs = {
            nfl: {
                profileDetails: {
                    name: "NFL Canada",
                    location: "Canada",
                    displayPic: "https://s0.wp.com/wp-content/themes/vip/nflcanada-twentyten/images/logo.png",
                    coverPhoto: "https://nflcanada.files.wordpress.com/2017/06/nfl_play60-map-1200x6281.jpg?w=400&h=300&crop=1",
                    website: "https://canada.nfl.com",
                    description: "The National Football League is America's most popular football league, comprises of 32 franchises that compete each year to win the Super Bowl, the world biggest sporting event. Founded in 1920, NFL developed the model of successful modern sports league."
                },
                stats: {
                    posts: 7000000,
                    followers: 17546,
                    following: 312
                },
                partners:[
                    {
                        "name": "visa",
                        "link": "",
                        "logo": ""
                    },
                    {
                        "name": "visa",
                        "link": "",
                        "logo": ""
                    },
                    {
                        "name": "visa",
                        "link": "",
                        "logo": ""
                    },
                    {
                        "name": "visa",
                        "link": "",
                        "logo": ""
                    }
                ]
            },
            manish: {
                profileDetails: {
                    name: "Manish ka Club",
                    location: "Indian",
                    displayPic: "https://s0.wp.com/wp-content/themes/vip/nflcanada-twentyten/images/logo.png",
                    coverPhoto: "https://nflcanada.files.wordpress.com/2017/06/nfl_play60-map-1200x6281.jpg?w=400&h=300&crop=1",
                    website: "https://manish.nfl.com",
                    description: "The National Football League is America's most popular football league, comprises of 32 franchises that compete each year to win the Super Bowl, the world biggest sporting event. Founded in 1920, NFL developed the model of successful modern sports league."
                },
                stats: {
                    posts: 7000000,
                    followers: 17546,
                    following: 312
                },
                partners:[
                    {
                        "name": "visa",
                        "link": "",
                        "logo": ""
                    },
                    {
                        "name": "visa",
                        "link": "",
                        "logo": ""
                    },
                    {
                        "name": "visa",
                        "link": "",
                        "logo": ""
                    },
                    {
                        "name": "visa",
                        "link": "",
                        "logo": ""
                    }
                ]
            }
        };
        this.state = {};
    }

    componentWillMount () {
        let clubName = this.props.match.params.name;
        let clubDetails = this.clubs[clubName]? this.clubs[clubName]: undefined;
        this.setState({
            clubDetails
        });
    }

    getPartnerProfile() {
        return (
            <div>
                <PartnerProfile
                    profileDetails={this.state.clubDetails.profileDetails}
                    stats={this.state.clubDetails.stats}
                    partners={this.state.clubDetails.partners}
                />
                <PartnerFeedPage
                    match={this.props.match} />
            </div>
        );
    }

    getPartnerNotFound() {
        return (
            <div className="partner-not-found">
                <h2>Partner not found</h2>
            </div>
        );
    }

    render () {
        return (
            <div>
                <Header>
                    <ProfileDetails
                        profileData={{
                            name: "NFL",
                            displayPic: ""
                        }}
                        notification={{
                            count: 0
                        }}
                    />
                    <div className="vertical-bar"></div>
                    <div className="explore">
                        <Link to={`/explore/${this.props.match.params.name}`}>EXPLORE</Link>
                    </div>
                    <div className="newsfeed">
                        <Link to={`/newsfeed/${this.props.match.params.name}`}>NEWSFEED</Link>
                    </div>
                    <SearchButton className="search-partners" height="24" width="24" color="whitesmoke"/>
                </Header>
                <div className="partners-container">
                    {
                        this.state.clubDetails?
                            this.getPartnerProfile() : this.getPartnerNotFound()
                    }
                </div>
            </div>
        );
    }
}

export default Partners;