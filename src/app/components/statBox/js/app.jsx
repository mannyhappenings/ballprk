import React, {Component} from 'react';
import '../css/app.less';

export default class StatBox extends Component {
    createMetric (stat, index, size) {
        return (
            <div key={index} className="stat-metric" style={{width: (100 / size) + "%", display: "inline-block"}}>
                <div className="metric">{stat.count}</div>
                <div className="label">{stat.label}</div>
            </div>
        );
    }

    render () {
        return (
            <div className="stat-box">
                <div className="metrics">
                    {
                        Object.keys(this.props.stats).map(key => ({label: key, count: this.props.stats[key]}))
                            .map((stat, i) => this.createMetric(stat, i, Object.keys(this.props.stats).length))
                    }
                </div>
                <div className="message-button">
                    <button>Message</button>
                </div>
            </div>
        );
    }
}