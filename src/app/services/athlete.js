const getAthletes = () => {
    return fetch("/athletes.json")
        .then(r => r.text())
        .then(r => JSON.parse(r));
}

export default {
    getAthletes
};